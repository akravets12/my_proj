/* global AFRAME */

/**
 * Component that listens to an event, fades out an entity, swaps the texture, and fades it
 * back in.
 */
 
AFRAME.registerComponent('set-image', {
  schema: {
    on: {type: 'string'},
    target: {type: 'selector'},
    src: {type: 'string'},
    dur: {type: 'number', default: 1000}
  },

  init: function () {
    var data = this.data;
    var el = this.el;

  this.setupFadeAnimation();

    el.addEventListener(data.on, function () {
      // Fade out image.
      
      // Wait for fade to complete.
   
        // Set image.
    data.target.emit('set-image-fade');
      setTimeout(function () {
     data.target.setAttribute('material', 'src', data.src);
  

        if(data.src == "#left")
        {
 


              var cursor1 = document.getElementById("cursor2");
                cursor1.setAttribute('raycaster', 'interval: 1000; objects: [gui-interactable]');
                cursor1.setAttribute('fuse', true);
                cursor1.setAttribute('fuse-timeout', 2000);
                cursor1.setAttribute('design', 'ring');

              var test = document.getElementById("test");
                test.setAttribute('visible', true);
              var test1 = document.getElementById("test1");
                test1.setAttribute('visible', true);
              
              var test2 = document.getElementById("test2");
                test2.setAttribute('visible',true);
              var test3 = document.getElementById("test3");
                test3.setAttribute('visible', true);


              document.getElementsByClassName("left-thumb-image")[0].setAttribute('visible',false);
              document.getElementsByClassName("center-thumb-image")[0].setAttribute('visible',true);
   // document.getElementsByClassName("menu")[0].setAttribute('visible',true);
  
              var bed = document.getElementById("beds");
                bed.setAttribute('visible', true);
              var cushion = document.getElementById("cushions");
                cushion.setAttribute('visible', true);
              var curtain = document.getElementById("curtains");
                curtain.setAttribute('visible', true);
    
              var leftl = document.getElementById("leftlink");
                leftl.setAttribute('template', 'src', '#link2');
              var centerl = document.getElementById("centerlink");
                centerl.setAttribute('template', 'src', '#link');
        
        }
        else if (data.src == "#center")
        {


              var test = document.getElementById("test");
                test.setAttribute('visible', false);
              var test1 = document.getElementById("test1");
                test1.setAttribute('visible', false);
                
              var test2 = document.getElementById("test2");
                test2.setAttribute('visible',false);
              var test3 = document.getElementById("test3");
                test3.setAttribute('visible', false);
  
  
              var cursor = document.getElementById("cursor2");
                cursor.removeAttribute("raycaster");
                cursor.removeAttribute("fuse");
                cursor.removeAttribute("fuse-timeout");
                cursor.removeAttribute("design");
  
      
  
              var bed = document.getElementById("beds");
                bed.setAttribute('visible', false);
                bed.setAttribute('src', '#transparent');
              var cushion = document.getElementById("cushions");
                cushion.setAttribute('visible', false);
                cushion.setAttribute('src', '#transparent');
              var curtain = document.getElementById("curtains");
                curtain.setAttribute('visible', false);
                curtain.setAttribute('src', '#transparent');
                 
  
  
              document.getElementsByClassName("left-thumb-image")[0].setAttribute('visible',true);
             // document.getElementsByClassName("menu")[0].setAttribute('visible',false);
              document.getElementsByClassName("center-thumb-image")[0].setAttribute('visible',false);
  
              var leftl = document.getElementById("leftlink");
                leftl.setAttribute('template', 'src', '#link');
              var centerl = document.getElementById("centerlink");
                centerl.setAttribute('template', 'src', '#link2');    
  
        }

           }, data.dur/2);
        setTimeout(function () {
            if(data.src == "#left"){
  document.getElementsByClassName("menu")[0].setAttribute('visible',true);
            }

              else if (data.src == "#center"){
       document.getElementsByClassName("menu")[0].setAttribute('visible',false);
     }
           }, data.dur*1.5);
    });
 
  },

  /**
   * Setup fade-in + fade-out.
   */
  setupFadeAnimation: function () {
    var data = this.data;
    var targetEl = this.data.target;

    // Only set up once.
    if (targetEl.dataset.setImageFadeSetup) { 
      return; }

    targetEl.dataset.setImageFadeSetup = true;

    // Create animation.
    
    targetEl.setAttribute('animation__fade', {
      property: 'material.color',
      startEvents: 'set-image-fade',
      dir: 'alternate',
      dur: data.dur,
      from: '#FFF',
      to: '#000'
    });
  }

    

}); 
 